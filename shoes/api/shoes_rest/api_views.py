from django.shortcuts import render
from common.json import ModelEncoder
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import BinVO, Shoe



class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model",
        "color",
        "picture_url",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }





@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_id = content["bin"]
            bin_href = f"/api/bins/{bin_id}/"
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status = 400,
            )

        shoes = Shoe.objects.create(**content)
        print(shoes)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_shoes(request, pk):
    if request.method == "GET":
        try:
            shoes = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoes,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status = 400,
            )

    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        try:
            content = json.loads(request.body)
            Shoe.objects.filter(id=pk).update(**content)
            shoes = Shoe.objects.get(id=pk)
            return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status = 400,
            )
