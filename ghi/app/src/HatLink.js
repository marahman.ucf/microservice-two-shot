import React, {useEffect, useState} from 'react';
import { NavLink } from "react-router-dom"

function HatLinks() {
    return(
      <div class="d-grid gap-2 col-6 mx-auto">
        <NavLink className="nav-link" aria-current="page" to="/">
          <button class="btn btn-primary" type="button">Create</button>
        </NavLink>
        <NavLink className="nav-link" aria-current="page" to="/hats/list">
          <button class="btn btn-primary" type="button">List</button>
        </NavLink>
        <NavLink className="nav-link" aria-current="page" to="/">
          <button class="btn btn-primary" type="button">Update</button>
        </NavLink>
        <NavLink className="nav-link" aria-current="page" to="/">
          <button class="btn btn-primary" type="button">Delete</button>
        </NavLink>
      </div>
    )
}
export default HatLinks;
