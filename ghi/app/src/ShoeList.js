function ShoeList(props) {

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>Picture url</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes && props.shoes.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.picture_url }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoeList;
