import { Link, NavLink } from 'react-router-dom';

function ShoeHome() {

    return (
    <div class="d-grid gap-2 col-6 mx-auto">
        <NavLink className="nav-link" aria-current="page" to="/">
          <button class="btn btn-primary" type="button">Create a new shoe item</button>
        </NavLink>
        <NavLink className="nav-link" aria-current="page" to="/shoes/list">
          <button class="btn btn-primary" type="button">List all the shoe items</button>
        </NavLink>
        <NavLink className="nav-link" aria-current="page" to="/">
          <button class="btn btn-primary" type="button">Update a shoe item</button>
        </NavLink>

    </div>
    );
  }

  export default ShoeHome;


   {/* <Link to="/ShoeList">
        <button class="btn btn-primary" type="button">List of shoes</button>
        </Link>

        <button class="btn btn-primary" type="button">Create a new shoe item</button>
        <button class="btn btn-primary" type="button">Edit a shoe item</button>
        <button class="btn btn-primary" type="button">Delete a shoe item</button> */}
