import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatLinks from './HatLink'
import HatList from './HatList'
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeHome from './ShoeHome';

function App(props) {
  if (props.shoes === undefined) {
    return null;
  }
  // if (props.hats === undefined) {
  //   return null;
  // }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        <Route index element={<MainPage />} />
        <Route path="/hats" element={<HatLinks />} />
        <Route path="/hats/list" element={<HatList hats={ props.hats }/>} />
          <Route index element={<MainPage />} />
          <Route path="/shoes" element={<ShoeHome />} />
          <Route path="/shoes/list" element={<ShoeList shoes={ props.shoes } />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
